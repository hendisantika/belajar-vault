package com.hendisantika.belajarvault

import com.winterbe.expekt.should
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment

@SpringBootTest
//@VaultPropertySource(value = "secret/belajar")
class BelajarVaultApplicationTests {

    @Autowired
    lateinit var environment: Environment

    private val logger = LoggerFactory.getLogger(BelajarVaultApplicationTests::class.java)

    @Test
    fun testUrl() {
        logger.info("begin test url")
        environment.getRequiredProperty("spring.datasource.url").should.equal("jdbc:mysql://localhost:3306/taxDB?createDatabaseIfNotExist=true&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Jakarta&useSSL=false&allowPublicKeyRetrieval=true")
        environment.getRequiredProperty("spring.datasource.url").should.not.`null`
        environment.getRequiredProperty("spring.datasource.url").should.not.`empty`
        logger.info("end test url")
    }

    @Test
    fun testUsername() {
        logger.info("begin test username")
        environment.getRequiredProperty("spring.datasource.username").should.equal("root")
        environment.getRequiredProperty("spring.datasource.username").should.not.`null`
        environment.getRequiredProperty("spring.datasource.username").should.not.`empty`
        logger.info("end test username")
    }

    @Test
    fun testPassword() {
        logger.info("begin test password")
        environment.getRequiredProperty("spring.datasource.password").should.equal("root")
        environment.getRequiredProperty("spring.datasource.password").should.not.`null`
        environment.getRequiredProperty("spring.datasource.password").should.not.`empty`
        logger.info("end test password")
    }

}
