# Spring Boot Vault

### What is HasiCorp Vault ?

HashiCorp Vault is an identity-based secrets and encryption management system.
A secret is anything that you want to tightly control access to, such as API encryption keys, passwords and
certificates.
Vault provides encryption services that are gated by authentication and authorization methods.

### Vault Server Setup

If you don’t have vault server please go to this official website to download and setup vault
server. https://developer.hashicorp.com/vault/downloads

Unzip the downloaded file you will get vault.exe, Now to use the vault commands you need to set the env. variable.

Once you set the env. variable you are good to go, to verify If you have done it successfully open you cmd/terminal
according to your operating system to verify.

Write below command on your terminal

```shell
vault version
```

Now you should see the result as shown in below screenshot.

```shell
Vault v1.16.1 (6b5986790d7748100de77f7f127119c4a0f78946), built 2024-04-03T12:35:53Z
```

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/belajar-vault.git`
2. Navigate to the folder: `cd belajar-vault`
3. Run the application: `gradle clean bootRun`
4. Check console

```shell
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.2.5)

2024-04-19T10:55:27.509+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] c.h.b.BelajarVaultApplicationKt          : Starting BelajarVaultApplicationKt using Java 21 with PID 81062 (/Users/hendisantika/IdeaProjects/belajar-vault/build/classes/kotlin/main started by hendisantika in /Users/hendisantika/IdeaProjects/belajar-vault)
2024-04-19T10:55:27.509+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] c.h.b.BelajarVaultApplicationKt          : No active profile set, falling back to 1 default profile: "default"
2024-04-19T10:55:27.527+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] o.s.v.c.e.LeaseAwareVaultPropertySource  : Vault location [kv/spring-boot-vault-integration-demo] not resolvable: Not found
2024-04-19T10:55:27.708+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] o.s.cloud.context.scope.GenericScope     : BeanFactory id=4cc94268-f1d6-398c-b68b-1db47eaa3354
2024-04-19T10:55:27.808+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] c.h.b.BelajarVaultApplicationKt          : Started BelajarVaultApplicationKt in 0.757 seconds (process running for 1.014)
2024-04-19T10:55:27.810+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] c.h.b.BelajarVaultApplication            : hasil secret dari url : jdbc:mysql://localhost:3306/taxDB?createDatabaseIfNotExist=true&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Jakarta&useSSL=false&allowPublicKeyRetrieval=true
2024-04-19T10:55:27.810+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] c.h.b.BelajarVaultApplication            : hasil secret dari username : root
2024-04-19T10:55:27.810+07:00  INFO 81062 --- [spring-boot-vault-integration-demo] [           main] c.h.b.BelajarVaultApplication            : hasil secret dari password : root
```
